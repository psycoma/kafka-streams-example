package config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import utils.JsonSerializer;

import java.util.Properties;

public class AppConfig {
    public static final String BOOTSTRAP_SERVER = "localhost:9092";

    public static final String GEO_INPUT;
    public static final String GEO_TO_ORDER;
    public static final String GEO_TO_USERINFO;

    public static final String USER_TO_ORDER_INPUT;
    public static final String ORDER_INPUT;
    public static final String ORDER_TO_HISTORY;

    public static final String HISTORY_INPUT;
    public static final String HISTORY_TO_USERINFO;

    public static final String USERINFO_INPUT;

    public static final String ENRICHED_USERS;

    static {
        GEO_INPUT = "geoEnrichment";
        GEO_TO_ORDER = USER_TO_ORDER_INPUT = "orderEnrichment";
        ORDER_INPUT = "order";
        ORDER_TO_HISTORY = HISTORY_INPUT = "historyEnrichment";
        GEO_TO_USERINFO = HISTORY_TO_USERINFO = USERINFO_INPUT = "userInfo";
        ENRICHED_USERS = "enrichedUsers";
    }

    public static Properties streamingProperties(String bootstrapServers, String applicationId) {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration
                .put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        streamsConfiguration
                .put(StreamsConfig.TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        streamsConfiguration
                .put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration
                .put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
  /*  streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, folder.newFolder("kafka-streams-tmp", applicationId)
        .getAbsolutePath());
*/
        return streamsConfiguration;
    }

    public static Properties producerProperties(String bootstrapServer, Class jsonValueType) {
        Properties producerConfig = new Properties();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
        producerConfig.put(ProducerConfig.RETRIES_CONFIG, 0);
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        producerConfig.put("JSONType", jsonValueType);

        return producerConfig;
    }
}
