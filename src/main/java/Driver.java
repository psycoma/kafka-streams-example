import domain.OrderInfo;
import domain.UserLog;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static config.AppConfig.BOOTSTRAP_SERVER;
import static config.AppConfig.GEO_INPUT;
import static config.AppConfig.ORDER_INPUT;
import static config.AppConfig.producerProperties;

public class Driver {

    static final List<String> userIds = Arrays.asList("Dmitry", "Vova", "Taras", "Stas");
    static final List<String> ips = Arrays.asList("195.138.67.100", "195.138.67.101", "195.138.67.102", "195.138.67.103");
    static final List<String> transactionIds = Arrays.asList("transaction1", "transaction2", "transaction3", null);
    static final List<String> prices = Arrays.asList("100", "200", "300", "400");
    static final Random random = new Random();

    public static void main(String[] args) throws InterruptedException {

        final KafkaProducer<String, UserLog> userLogProducer = new KafkaProducer<>(producerProperties(BOOTSTRAP_SERVER, UserLog.class));
        final KafkaProducer<String, OrderInfo> orderInfoProducer = new KafkaProducer<>(producerProperties(BOOTSTRAP_SERVER, OrderInfo.class));

        while (true) {
            int index = random.ints(0, 4).findAny().getAsInt();

            final UserLog userLog = userLog(index);
            final OrderInfo orderInfo = orderInfo(index);

            System.out.println("Sending user: " + userLog);
            userLogProducer.send(new ProducerRecord<>(GEO_INPUT, userLog));

            if (orderInfo.getTransactionId() != null) {
                System.out.println("Sending order: " + orderInfo);
                orderInfoProducer.send(new ProducerRecord<>(ORDER_INPUT, orderInfo.getTransactionId(), orderInfo));
            }

            Thread.sleep(1000L);
        }
    }

    private static OrderInfo orderInfo(int index) {
        return new OrderInfo(transactionIds.get(index), prices.get(index));
    }

    private static UserLog userLog(int index) {
        return new UserLog(userIds.get(index),
                ips.get(index),
                transactionIds.get(index));
    }
}

