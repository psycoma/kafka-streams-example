package utils;

import java.util.Map;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by dkarpov on 3/23/17.
 */
public class JsonDeserializer<T> implements Deserializer<T> {
  private final ObjectMapper mapper = new ObjectMapper();
  private Class<T> clazz;

  public JsonDeserializer(Class<T> clazz) {
    this.clazz = clazz;
  }

  public JsonDeserializer() {

  }

  @Override public void configure(Map<String, ?> configs, boolean isKey) {
   clazz = (Class<T>) configs.get("JSONType");
  }

  @Override public T deserialize(String topic, byte[] data) {
    try {
      if (data == null) {
        return null;
      } else {
        return mapper.readValue(data, clazz);
      }
    } catch (Exception e) {
      throw new SerializationException(e);
    }
  }

  @Override public void close() {

  }
}
