package utils;

import java.util.Map;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonPOJOSerde<T> implements Serde<T> {

  private final Class<T> cls;

  public JsonPOJOSerde(Class<T> cls) {
    this.cls = cls;
  }

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {

  }

  @Override
  public void close() {

  }

  @Override
  public Serializer<T> serializer() {
    return new JsonSerializer<T>(cls);
  }

  @Override
  public Deserializer<T> deserializer() {
    return new JsonDeserializer(cls);
  }

}
