package utils;

import java.util.Map;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonSerializer<T> implements Serializer<T>{
  private final ObjectMapper mapper = new ObjectMapper();
  private Class<T> clazz;

  public JsonSerializer(Class<T> cls) {
    this.clazz = cls;
  }

  public JsonSerializer(){

  }

  @Override public void configure(Map<String, ?> configs, boolean isKey) {
    this.clazz = (Class<T>) configs.get("JSONType");
  }

  @Override public byte[] serialize(String topic, T data) {
    try {
      return mapper.writeValueAsBytes(data);
    } catch (Exception e) {
      throw new SerializationException("Error serializing JSON message", e);
    }
  }

  @Override public void close() {

  }
}
