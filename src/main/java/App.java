import org.apache.kafka.streams.KafkaStreams;
import pipeline.GeoInformationPipeline;
import pipeline.UserHistoryPipeline;
import pipeline.UserInfoPipeline;
import pipeline.UserOrdersPipeline;
import service.UserInfoRestService;

import java.util.UUID;

import static config.AppConfig.BOOTSTRAP_SERVER;
import static config.AppConfig.GEO_INPUT;
import static config.AppConfig.GEO_TO_ORDER;
import static config.AppConfig.GEO_TO_USERINFO;
import static config.AppConfig.HISTORY_INPUT;
import static config.AppConfig.HISTORY_TO_USERINFO;
import static config.AppConfig.ORDER_INPUT;
import static config.AppConfig.ORDER_TO_HISTORY;
import static config.AppConfig.USERINFO_INPUT;
import static config.AppConfig.USER_TO_ORDER_INPUT;
import static config.AppConfig.streamingProperties;

public class App {
    public static void main(String[] args) throws Exception {

        KafkaStreams geoInfoStream = GeoInformationPipeline
                .construct(GEO_INPUT,
                        GEO_TO_ORDER,
                        GEO_TO_USERINFO,
                        streamingProperties(BOOTSTRAP_SERVER, UUID.randomUUID().toString()));

        KafkaStreams userOrdersStream = UserOrdersPipeline.construct(USER_TO_ORDER_INPUT,
                ORDER_INPUT,
                ORDER_TO_HISTORY,
                streamingProperties(BOOTSTRAP_SERVER, UUID.randomUUID().toString()));

        KafkaStreams userHistoryStream =
                UserHistoryPipeline.construct(HISTORY_INPUT,
                        HISTORY_TO_USERINFO,
                        streamingProperties(BOOTSTRAP_SERVER, UUID.randomUUID().toString()));

        KafkaStreams userInfoStream =
                UserInfoPipeline
                        .construct(USERINFO_INPUT,
                                streamingProperties(BOOTSTRAP_SERVER, UUID.randomUUID().toString()));

        geoInfoStream.start();
        userOrdersStream.start();
        userHistoryStream.start();
        userInfoStream.start();

        UserInfoRestService userInfoRestService = new UserInfoRestService(userInfoStream);

        userInfoRestService.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                userInfoRestService.stop();
                geoInfoStream.close();
                userOrdersStream.close();
                userHistoryStream.close();
                userInfoStream.close();
            } catch (Exception e) {
                // ignored
            }
        }));
    }

}
