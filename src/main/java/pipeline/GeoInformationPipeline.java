package pipeline;

import domain.EnrichedUserLog;
import domain.UserLog;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import store.GeoLocationStoreSupplier;
import transformer.GeoLocationTransformer;
import utils.JsonPOJOSerde;

import java.util.Properties;

public class GeoInformationPipeline {
  public static KafkaStreams construct(String userActivityLogTopic,
      String orderInfoTopic,
      String outputTopic,
      Properties config) {

    KStreamBuilder builder = new KStreamBuilder();

    builder.addStateStore(new GeoLocationStoreSupplier());

    KStream<String, UserLog> userLog = builder.stream(Serdes.String(),
        new JsonPOJOSerde<>(UserLog.class),
        userActivityLogTopic);

    userLog.print("Input");

    KStream<String, EnrichedUserLog> enrichedUserLog = userLog
        .selectKey((key, value) -> value.getUserId())
        .mapValues(EnrichedUserLog::new)
        .transform(GeoLocationTransformer::new, "GeoStore");

    enrichedUserLog.print("Enriched with Geo");

    KStream<String, EnrichedUserLog> withTxId = enrichedUserLog
        .filter((key, value) -> value.getTransactionId() != null)
        .selectKey((key, value) -> value.getTransactionId());

    withTxId.print("Records with tx id");

    withTxId.to(Serdes.String(), new JsonPOJOSerde<>(EnrichedUserLog.class), orderInfoTopic);

    KStream<String, EnrichedUserLog> withoutTxId = enrichedUserLog
        .filter((key, value) -> value.getTransactionId() == null);

    withoutTxId.print("Records without tx id");

    withoutTxId
        .to(Serdes.String(), new JsonPOJOSerde<>(EnrichedUserLog.class), outputTopic);

    return new KafkaStreams(builder, config);
  }

}
