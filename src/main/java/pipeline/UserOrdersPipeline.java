package pipeline;

import domain.EnrichedUserLog;
import domain.OrderInfo;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.KTable;
import processor.StoreCleanerSupplier;
import utils.JsonPOJOSerde;

import java.util.Properties;

public class UserOrdersPipeline {
  private static final String USER_LOG_STORE = "userLog";

  public static KafkaStreams construct(String userLogTopic, String userOrderInfo,
      String output,
      Properties config) {
    KStreamBuilder builder = new KStreamBuilder();

    KTable<String, EnrichedUserLog> userLogTable = builder.table(Serdes.String(),
        new JsonPOJOSerde<>(EnrichedUserLog.class),
        userLogTopic,
        USER_LOG_STORE);

    userLogTable.foreach((key, value) -> System.out.println(value));

    KStream<String, OrderInfo> ordersStream = builder.stream(Serdes.String(),
        new JsonPOJOSerde<>(OrderInfo.class),
        userOrderInfo);

    ordersStream.print("Orders");

    KStream<String, EnrichedUserLog> enrichedWithOrderDetails = ordersStream
        .join(userLogTable,
            (orderInfo, userLog) -> {
              userLog.setOrderPrice(orderInfo.getOrderPrice());
              return userLog;
            });

    enrichedWithOrderDetails.print("Joined records");

    //cleaning store after the join
    enrichedWithOrderDetails
        .process(new StoreCleanerSupplier(USER_LOG_STORE), USER_LOG_STORE);

    enrichedWithOrderDetails
        .to(Serdes.String(),
            new JsonPOJOSerde<>(EnrichedUserLog.class),
            output);

    return new KafkaStreams(builder, config);
  }

}
