package pipeline;

import domain.EnrichedUserLog;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import utils.JsonPOJOSerde;

import java.util.Properties;

import static config.AppConfig.ENRICHED_USERS;

public class UserInfoPipeline {

    public static KafkaStreams construct(String inputTopic, Properties properties) {

        KStreamBuilder builder = new KStreamBuilder();

        KStream<String, EnrichedUserLog> users = builder.stream(Serdes.String(),
                new JsonPOJOSerde<>(EnrichedUserLog.class),
                inputTopic);

        users.to(Serdes.String(), new JsonPOJOSerde<>(EnrichedUserLog.class), ENRICHED_USERS);

        builder.table(Serdes.String(),
                new JsonPOJOSerde<>(EnrichedUserLog.class),
                ENRICHED_USERS,
                "enrichedUsers");

        KStream<String, EnrichedUserLog> usersWithOrderes = users.filter((key, value) -> value.getTransactionId() != null);

        KStream<String, EnrichedUserLog> usersWithoutOrders = users.filter((key, value) -> value.getTransactionId() == null);

        usersWithOrderes.print("users with orders");

        usersWithoutOrders.print("users without orders");

        usersWithOrderes.selectKey((k, v) -> v.getUserId())
                .groupByKey(Serdes.String(), new JsonPOJOSerde<>(EnrichedUserLog.class))
                .count("usersCount")
                .toStream()
                .print("Users count");

        KGroupedStream<String, EnrichedUserLog> groupedByCities = usersWithOrderes.selectKey((k, v) -> v.getCity())
                .groupByKey(Serdes.String(), new JsonPOJOSerde<>(EnrichedUserLog.class));

        KGroupedStream<String, EnrichedUserLog> groupedByCitiesWithoutTx = usersWithoutOrders.selectKey((k, v) -> v.getCity())
                .groupByKey(Serdes.String(), new JsonPOJOSerde<>(EnrichedUserLog.class));

        groupedByCities.count("usersActivityPerCity")
                .toStream()
                .print("Users activity per city");

        groupedByCities.aggregate(() -> 0L,
                (key, value, agg) -> agg + Long.parseLong(value.getOrderPrice()),
                Serdes.Long(),
                "moneyPerCity")
                .toStream()
                .print("Money per city");

        groupedByCitiesWithoutTx.aggregate(() -> 0L,
                (key, value, agg) -> value.getTransactionId() == null ? agg + 1 : agg,
                Serdes.Long(),
                "usersActivityWithoutPurchase")
                .toStream()
                .print("Users activity without purchase");

        return new KafkaStreams(builder, properties);
    }

}
