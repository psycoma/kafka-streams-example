package pipeline;

import avro.shaded.com.google.common.collect.Sets;
import com.google.common.collect.ImmutableMap;
import domain.EnrichedUserLog;
import domain.UserOrdersHistory;
import lookup.UserHistoryLookupClient;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.state.Stores;
import store.UserHistoryStoreSupplier;
import utils.JsonPOJOSerde;

import java.util.Properties;

public class UserHistoryPipeline {
    private static final String HISTORY_RESOLVER_STORE = "historyResolverStore";
    private static final String HISTORY_RESOLVER_PROCESSOR = "historyResolverProcessor";
    private static final String SOURCE = "SOURCE";
    private static final String OUTPUT = "OUTPUT";

    public static KafkaStreams construct(String input, String output,
                                         Properties config) {

        KStreamBuilder builder = new KStreamBuilder();

        StringSerializer stringSerializer = new StringSerializer();
        StringDeserializer stringDeserializer = new StringDeserializer();

        Serializer<EnrichedUserLog> jsonSerializer = new JsonPOJOSerde<>(EnrichedUserLog.class)
                .serializer();
        Deserializer<EnrichedUserLog> jsonDeserializer = new JsonPOJOSerde<>(EnrichedUserLog.class)
                .deserializer();

        builder
                .addSource(SOURCE, stringDeserializer, jsonDeserializer, input)
                .addProcessor(HISTORY_RESOLVER_PROCESSOR, new UserHistoryStoreSupplier(HISTORY_RESOLVER_STORE,
                        new UserHistoryLookupClient(ImmutableMap.<String, UserOrdersHistory>builder()
                                //put hardcoded value just for test purpose
                                .put("Vova", new UserOrdersHistory("Vova", Sets.newHashSet("Shoes", "Pants")))
                                .put("Dmitry", new UserOrdersHistory("Dmitry", Sets.newHashSet("Book", "Beer")))
                                .put("Taras", new UserOrdersHistory("Taras", Sets.newHashSet("Vodka", "Wine")))
                                .put("Stas", new UserOrdersHistory("Stas", Sets.newHashSet("Pizza", "Steak")))
                                .build())
                ), SOURCE)
                .addStateStore(Stores.create(HISTORY_RESOLVER_STORE)
                        .withStringKeys()
                        .withValues(new JsonPOJOSerde<>(EnrichedUserLog.class))
                        .inMemory()
                        .maxEntries(3000)
                        .build(), HISTORY_RESOLVER_PROCESSOR)
                .addSink(OUTPUT, output, stringSerializer, jsonSerializer, HISTORY_RESOLVER_PROCESSOR);

        return new KafkaStreams(builder, config);
    }

}
