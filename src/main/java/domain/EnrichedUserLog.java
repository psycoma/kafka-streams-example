package domain;

public class EnrichedUserLog {
  private String userId;
  private String ip;
  private String city;
  private String orderPrice;
  private UserOrdersHistory userOrdersHistory;
  private String transactionId;

  public EnrichedUserLog() {
  }

  public EnrichedUserLog (UserLog userLog) {
    this.userId = userLog.getUserId();
    this.ip = userLog.getIp();
    this.transactionId = userLog.getTransactionId();
  }

  public EnrichedUserLog(String userId, String ip, String city, String orderPrice, String transactionId) {
    this.userId = userId;
    this.ip = ip;
    this.city = city;
    this.orderPrice = orderPrice;
    this.transactionId = transactionId;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getOrderPrice() {
    return orderPrice;
  }

  public void setOrderPrice(String orderPrice) {
    this.orderPrice = orderPrice;
  }

  public UserOrdersHistory getUserOrdersHistory() {
    return userOrdersHistory;
  }

  public void setUserOrdersHistory(UserOrdersHistory userOrdersHistory) {
    this.userOrdersHistory = userOrdersHistory;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  @Override public String toString() {
    return "EnrichedUserLog{" +
        "userId='" + userId + '\'' +
        ", ip='" + ip + '\'' +
        ", city='" + city + '\'' +
        ", orderPrice='" + orderPrice + '\'' +
        ", userOrdersHistory=" + userOrdersHistory +
        ", transactionId='" + transactionId + '\'' +
        '}';
  }
}
