package domain;

public class UserLog {
  private String userId;
  private String ip;
  private String transactionId;

  public UserLog() {
  }

  public UserLog(String userId, String ip, String transactionId) {
    this.userId = userId;
    this.ip = ip;
    this.transactionId = transactionId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  @Override public String toString() {
    return "UserLog{" +
        "userId='" + userId + '\'' +
        ", ip='" + ip + '\'' +
        ", transactionId='" + transactionId + '\'' +
        '}';
  }
}
