package domain;

public class OrderInfo {
  private String transactionId;
  private String orderPrice;

  public OrderInfo(String transactionId, String orderPrice) {
    this.transactionId = transactionId;
    this.orderPrice = orderPrice;
  }

  public OrderInfo() {
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getOrderPrice() {
    return orderPrice;
  }

  public void setOrderPrice(String advertisimentCostAmount) {
    this.orderPrice = advertisimentCostAmount;
  }

  @Override public String toString() {
    return "OrderInfo{" +
        "transactionId='" + transactionId + '\'' +
        ", orderPrice='" + orderPrice + '\'' +
        '}';
  }
}
