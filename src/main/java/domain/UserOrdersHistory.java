package domain;

import java.util.Set;

public class UserOrdersHistory {
  private String userId;
  private Set<String> orders;

  public UserOrdersHistory() {
  }

  public UserOrdersHistory(String userId, Set<String> orders) {
    this.userId = userId;
    this.orders = orders;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Set<String> getOrders() {
    return orders;
  }

  public void setOrders(Set<String> orders) {
    this.orders = orders;
  }
}
