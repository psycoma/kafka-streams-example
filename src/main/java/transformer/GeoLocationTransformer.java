package transformer;

import domain.EnrichedUserLog;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

public class GeoLocationTransformer
        implements Transformer<String, EnrichedUserLog, KeyValue<String, EnrichedUserLog>> {
    KeyValueStore<String, String> stateStore;

    @Override
    public void init(ProcessorContext context) {
        stateStore = (KeyValueStore<String, String>) context.getStateStore("GeoStore");

        stateStore.put("195.138.67.100", "Odessa");
        stateStore.put("195.138.67.101", "Lviv");
        stateStore.put("195.138.67.102", "Kiev");
        stateStore.put("195.138.67.103", "Kharkiv");
    }

    @Override
    public KeyValue<String, EnrichedUserLog> transform(String key, EnrichedUserLog value) {
        String city = stateStore.get(value.getIp());

        value.setCity(city == null ? "UNKNOWN" : city);

        return new KeyValue<>(key, value);
    }

    @Override
    public KeyValue<String, EnrichedUserLog> punctuate(long timestamp) {
        return null;
    }

    @Override
    public void close() {
        stateStore.close();
    }
}
