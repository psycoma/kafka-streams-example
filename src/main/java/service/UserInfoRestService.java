package service;

import com.google.common.collect.Lists;
import domain.EnrichedUserLog;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("user-info")
public class UserInfoRestService {
    private Server jettyServer;
    private KafkaStreams streams;

    public UserInfoRestService(final KafkaStreams streams) {
        this.streams = streams;
    }

    @GET
    @Path("/count")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KeyValue<String, Long>> count() {
        ReadOnlyKeyValueStore<String, Long> usersCount = streams.store("usersCount", QueryableStoreTypes.<String, Long>keyValueStore());

        return Lists.newArrayList(usersCount.all());
    }

    @GET
    @Path("/activityPerCity")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KeyValue<String, Long>> activityPerCity() {
        ReadOnlyKeyValueStore<String, Long> usersActivityPerCity = streams.store("usersActivityPerCity", QueryableStoreTypes.<String, Long>keyValueStore());

        return Lists.newArrayList(usersActivityPerCity.all());
    }

    @GET
    @Path("/enrichedUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KeyValue<String, EnrichedUserLog>> enrichedUsers() {
        ReadOnlyKeyValueStore<String, EnrichedUserLog> enrichedUsers = streams.store("enrichedUsers", QueryableStoreTypes.<String, EnrichedUserLog>keyValueStore());

        return Lists.newArrayList(enrichedUsers.all());
    }

    @GET
    @Path("/moneyPerCity")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KeyValue<String, Long>> moneyPerCity() {
        ReadOnlyKeyValueStore<String, Long> moneyPerCity = streams.store("moneyPerCity", QueryableStoreTypes.<String, Long>keyValueStore());

        return Lists.newArrayList(moneyPerCity.all());
    }

    @GET
    @Path("/usersActivityWithoutPurchase")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KeyValue<String, Long>> usersActivityWithoutPurchase() {
        ReadOnlyKeyValueStore<String, Long> usersActivityWithoutPurchase = streams.store("usersActivityWithoutPurchase", QueryableStoreTypes.<String, Long>keyValueStore());

        return Lists.newArrayList(usersActivityWithoutPurchase.all());
    }

    public void start() throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        jettyServer = new Server(7070);
        jettyServer.setHandler(context);

        ResourceConfig rc = new ResourceConfig();
        rc.register(this);
        rc.register(JacksonFeature.class);

        ServletContainer sc = new ServletContainer(rc);
        ServletHolder holder = new ServletHolder(sc);
        context.addServlet(holder, "/*");

        jettyServer.start();
    }

    public void stop() throws Exception {
        if (jettyServer != null) {
            jettyServer.stop();
        }
    }
}
