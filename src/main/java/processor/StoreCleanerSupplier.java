package processor;

import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.state.KeyValueStore;

public class StoreCleanerSupplier<UserLog> implements ProcessorSupplier<String, UserLog> {
  private final String storeName;

  public StoreCleanerSupplier(String storeName) {
    this.storeName = storeName;
  }

  @Override public Processor<String, UserLog> get() {
    return new Processor<String, UserLog>() {

      private KeyValueStore<String, UserLog> store;

      @Override public void init(ProcessorContext context) {
        store = (KeyValueStore<String, UserLog>) context.getStateStore(storeName);
      }

      @Override public void process(String key, UserLog value) {
        store.delete(key);
      }

      @Override public void punctuate(long timestamp) {
      }

      @Override public void close() {
        store.close();
      }
    };
  }
}
