package store;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;

import com.google.common.collect.Maps;

import domain.EnrichedUserLog;
import domain.UserOrdersHistory;
import lookup.UserHistoryLookupClient;

public class UserHistoryStoreSupplier implements ProcessorSupplier<String, EnrichedUserLog> {
  public static final String OUTPUT_SINK = "OUTPUT";
  private final String storeName;
  private final UserHistoryLookupClient client;

  public UserHistoryStoreSupplier(String storeName, UserHistoryLookupClient lookup) {
    this.storeName = storeName;
    this.client = lookup;
  }

  @Override public Processor<String, EnrichedUserLog> get() {
    return new Processor<String, EnrichedUserLog>() {

      private KeyValueStore<String, EnrichedUserLog> store;
      private ProcessorContext context;

      @Override public void init(ProcessorContext context) {
        this.store =
            (KeyValueStore<String, EnrichedUserLog>) context.getStateStore(storeName);
        this.context = context;
        context.schedule(TimeUnit.SECONDS.toSeconds(10));
      }

      @Override public void process(String key, EnrichedUserLog value) {
        System.out.println(value);
        store.put(key, value);
      }

      @Override public void punctuate(long timestamp) {
        KeyValueIterator<String, EnrichedUserLog> iterator = store.all();

        Map<String, KeyValue<String, EnrichedUserLog>> batch = Maps.newHashMap();

        while (iterator.hasNext()) {
          KeyValue<String, EnrichedUserLog> kv = iterator.next();
          batch.put(kv.value.getUserId(), kv);
        }

        iterator.close();

        Map<String, UserOrdersHistory> resolvedHistory =
            client.historyByIds(batch.keySet());

        resolvedHistory.entrySet().stream().forEach(entry -> {
              Optional<KeyValue<String, EnrichedUserLog>> optional = Optional.of(batch.get(entry.getKey()));
              optional.ifPresent(kv -> kv.value.setUserOrdersHistory(entry.getValue()));
            }
        );

        batch.values().stream().forEach(kv -> {
          context.forward(kv.key, kv.value, OUTPUT_SINK);
          System.out.println(kv.value);
          store.delete(kv.key);
        });

      }

      @Override public void close() {
        store.close();
      }
    };
  }
}

