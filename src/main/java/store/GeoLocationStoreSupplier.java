package store;

import java.util.Map;

import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.processor.StateStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;

public class GeoLocationStoreSupplier implements StateStoreSupplier {

  public static final String GEO_STORE = "GeoStore";

  @Override public String name() {
    return GEO_STORE;
  }

  @Override
  public StateStore get() {
    KeyValueStore store = (KeyValueStore) Stores.create(GEO_STORE)
        .withStringKeys()
        .withStringValues()
        .inMemory()
        .maxEntries(3000)
        .disableLogging()
        .build().get();

    return store;
  }

  @Override public Map<String, String> logConfig() {
    return null;
  }

  @Override public boolean loggingEnabled() {
    return false;
  }
}
