package lookup;

import domain.UserOrdersHistory;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

//for test purposes only
public class UserHistoryLookupClient {

  private Map<String, UserOrdersHistory> storage;

  public UserHistoryLookupClient(Map<String, UserOrdersHistory> storage) {
    this.storage = storage;
  }

  //for test purposes only
  public Map<String, UserOrdersHistory> historyByIds(Set<String> ids) {
    return storage
            .entrySet()
            .stream()
            .filter(entry -> ids.contains(entry.getKey()))
            .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
  }
}
