package pipeline;

import domain.EnrichedUserLog;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.junit.Test;
import utils.IntegrationTestUtils;
import utils.StreamingTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static config.AppConfig.HISTORY_INPUT;
import static config.AppConfig.HISTORY_TO_USERINFO;
import static config.AppConfig.producerProperties;
import static config.AppConfig.streamingProperties;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class HistoryEnrichmentPipelineTest extends StreamingTest {

  @Test
  public void historyEnrichmentTest() throws IOException, ExecutionException, InterruptedException {
    KafkaStreams streams = UserHistoryPipeline
        .construct(HISTORY_INPUT, HISTORY_TO_USERINFO, streamingProperties(CLUSTER.bootstrapServers(),
                UUID.randomUUID().toString()));

    streams.start();

    publishEnrichedUserLog();

    KeyValue<String, EnrichedUserLog> enrichedLog = waitForLogsWithHistory().get(0);

    streams.close();

    assertThat("History orders should be populated", enrichedLog.value.getUserOrdersHistory(), is(notNullValue()));
  }

  private void publishEnrichedUserLog() throws ExecutionException, InterruptedException {
    IntegrationTestUtils.produceKeyValuesSynchronously(HISTORY_INPUT,
        Arrays.asList(new KeyValue<>("transactionId", new EnrichedUserLog("Vova", "127.0.0.1", "Odessa", "100" , "transactionId"))),
        producerProperties(CLUSTER.bootstrapServers(), EnrichedUserLog.class));
  }

  private List<KeyValue<String, EnrichedUserLog>> waitForLogsWithHistory()
      throws InterruptedException {
    return IntegrationTestUtils
        .waitUntilMinKeyValueRecordsReceived(
            getConsumerProperties("enriched log consumer",
                EnrichedUserLog.class),
                HISTORY_TO_USERINFO, 1);

  }
}
