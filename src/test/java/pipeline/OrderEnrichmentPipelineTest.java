package pipeline;

import domain.EnrichedUserLog;
import domain.OrderInfo;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.junit.Test;
import utils.IntegrationTestUtils;
import utils.StreamingTest;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static config.AppConfig.ORDER_INPUT;
import static config.AppConfig.ORDER_TO_HISTORY;
import static config.AppConfig.USER_TO_ORDER_INPUT;
import static config.AppConfig.producerProperties;
import static config.AppConfig.streamingProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class OrderEnrichmentPipelineTest extends StreamingTest {

  @Test
  public void orderPipelineTest() throws Exception {
    KafkaStreams stream =
        UserOrdersPipeline
            .construct(USER_TO_ORDER_INPUT,
                ORDER_INPUT,
                ORDER_TO_HISTORY,
                streamingProperties(CLUSTER.bootstrapServers(), UUID.randomUUID().toString()));

    stream.start();

    publishEnrichedUserLog();
    publishOrders();

    KeyValue<String, EnrichedUserLog> logWithOrderInfo = waitForLogWithOrderEnriched().get(0);

    stream.close();

    assertThat("Order price is populated and equals to 100", logWithOrderInfo.value.getOrderPrice(), is("100"));
  }

  private void publishEnrichedUserLog() throws ExecutionException, InterruptedException {
    IntegrationTestUtils.produceKeyValuesSynchronously(USER_TO_ORDER_INPUT,
        Arrays.asList(new KeyValue<>("transactionId", new EnrichedUserLog("Dima", "127.0.0.1", "Odessa", null , "transactionId"))),
        producerProperties(CLUSTER.bootstrapServers(), EnrichedUserLog.class));
  }

  private void publishOrders()
      throws java.util.concurrent.ExecutionException, InterruptedException {
    IntegrationTestUtils.produceKeyValuesSynchronously(ORDER_INPUT,
        Arrays.asList(new KeyValue<>("transactionId", new OrderInfo("transactionId", "100"))),
        producerProperties(CLUSTER.bootstrapServers(), OrderInfo.class));
  }

  private List<KeyValue<String, EnrichedUserLog>> waitForLogWithOrderEnriched()
      throws InterruptedException {
    return IntegrationTestUtils
        .waitUntilMinKeyValueRecordsReceived(
            getConsumerProperties("enriched log consumer",
                EnrichedUserLog.class),
            ORDER_TO_HISTORY, 1);

  }
}
