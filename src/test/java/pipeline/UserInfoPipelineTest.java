package pipeline;

import domain.EnrichedUserLog;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.junit.Test;
import utils.IntegrationTestUtils;
import utils.StreamingTest;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static config.AppConfig.USERINFO_INPUT;
import static config.AppConfig.producerProperties;
import static config.AppConfig.streamingProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UserInfoPipelineTest extends StreamingTest {

  @Test
  public void userInfoTest() throws Exception {
    KafkaStreams userInfo =
        UserInfoPipeline.construct(USERINFO_INPUT, streamingProperties(CLUSTER.bootstrapServers(), UUID.randomUUID().toString()));

    publishLogsToDashboard();

    userInfo.start();

    Thread.sleep(TimeUnit.SECONDS.toMillis(10));

    long dimasCount = userInfo
        .store("usersCount", QueryableStoreTypes.<String, Long>keyValueStore())
        .get("Dima");

    long cityActivityCount = userInfo
        .store("usersActivityPerCity",
            QueryableStoreTypes.<String, Long>keyValueStore())
        .get("Lviv");

    long moneyPerCity = userInfo
        .store("moneyPerCity",
            QueryableStoreTypes.<String, Long>keyValueStore())
        .get("Kiev");

    long nonActivityCount = userInfo
        .store("usersActivityWithoutPurchase", QueryableStoreTypes.<String, Long>keyValueStore())
        .get("Odessa");

    Thread.sleep(TimeUnit.SECONDS.toMillis(5));

    userInfo.close();

    assertThat("Dimas count with purshase should be 2", dimasCount, is(2l));
    assertThat("User activity in Lviv should be 1", cityActivityCount, is(1l));
    assertThat("Money amount in Kiev should be 200", moneyPerCity, is(200l));
    assertThat("Odessa should have 1 user without purshase", nonActivityCount, is(1l));

  }

  private void publishLogsToDashboard() throws ExecutionException, InterruptedException {
    IntegrationTestUtils.produceValuesSynchronously(USERINFO_INPUT,
        prepareEnrichedLogs(),
        producerProperties(CLUSTER.bootstrapServers(), EnrichedUserLog.class));
  }

  private Collection<EnrichedUserLog> prepareEnrichedLogs() {
    return Arrays.asList(
        new EnrichedUserLog("Dima", "127.0.0.1", "Odessa", "100", UUID.randomUUID().toString()),
        new EnrichedUserLog("Dima", "127.0.0.1", "Odessa", "100", UUID.randomUUID().toString()),
        new EnrichedUserLog("Poor Dima", "127.0.0.1", "Odessa", "0", null),
        new EnrichedUserLog("Kolya", "127.0.0.2", "Kiev", "200", UUID.randomUUID().toString()),
        new EnrichedUserLog("Taras", "127.0.0.3", "Lviv", "100", UUID.randomUUID().toString()),
        new EnrichedUserLog("Vasya", "127.0.0.4", "Odessa", "100", UUID.randomUUID().toString()));
  }

}
