package pipeline;

import domain.EnrichedUserLog;
import domain.UserLog;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.junit.Test;
import utils.IntegrationTestUtils;
import utils.StreamingTest;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static config.AppConfig.GEO_INPUT;
import static config.AppConfig.GEO_TO_ORDER;
import static config.AppConfig.GEO_TO_USERINFO;
import static config.AppConfig.producerProperties;
import static config.AppConfig.streamingProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

public class GeoEnrichmentPipelineTest extends StreamingTest {

  @Test
  public void geoEnrichmentTest() throws Exception {
    KafkaStreams userActivityStream = GeoInformationPipeline
        .construct(GEO_INPUT,
                GEO_TO_ORDER,
                GEO_TO_USERINFO,
                streamingProperties(CLUSTER.bootstrapServers(), UUID.randomUUID().toString()));

    userActivityStream.start();
    publishActivityLogs();

    KeyValue<String, EnrichedUserLog> withoutTx = waitForLogWithoutTx().get(0);

    KeyValue<String, EnrichedUserLog> withTx = waitForLogWithTx().get(0);

    assertThat("Record that has no transactionId should arrive", withoutTx.value.getTransactionId(), is(nullValue()));
    assertThat("Geo information should be populated", withoutTx.value.getCity(),
        is("Odessa"));
    assertThat("Key must be equal to userId", withoutTx.key,
        is(withoutTx.value.getUserId()));

    assertThat("Record that has tx Id should arrive", withTx.value.getTransactionId(), is(notNullValue()));
    assertThat("Key must be equal to txId", withTx.key, is(withTx.value.getTransactionId()));
    assertThat("Geo information should be populated", withTx.value.getCity(), is("Lviv"));

    userActivityStream.close();
  }

  private void publishActivityLogs() throws ExecutionException, InterruptedException {
    IntegrationTestUtils.produceValuesSynchronously(GEO_INPUT,
        prepareLogs(),
        producerProperties(CLUSTER.bootstrapServers(), UserLog.class));
  }

  private Collection<UserLog> prepareLogs() {
    return Arrays.asList(new UserLog("Dima", "195.138.67.100", null),
        new UserLog("Vova", "195.138.67.101", UUID.randomUUID().toString()));
  }

  private List<KeyValue<String, EnrichedUserLog>> waitForLogWithoutTx()
      throws InterruptedException {
    return IntegrationTestUtils
        .waitUntilMinKeyValueRecordsReceived(
            getConsumerProperties("enriched log consumer",
                EnrichedUserLog.class),
                GEO_TO_USERINFO, 1);

  }

  private List<KeyValue<String, EnrichedUserLog>> waitForLogWithTx()
      throws InterruptedException {
    return IntegrationTestUtils
        .waitUntilMinKeyValueRecordsReceived(
            getConsumerProperties("enriched log consumer",
                EnrichedUserLog.class),
            GEO_TO_ORDER, 1);
  }
}
