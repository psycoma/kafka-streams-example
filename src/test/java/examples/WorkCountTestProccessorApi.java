package examples;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.processor.StateStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import kafka.EmbeddedSingleNodeKafkaCluster;
import utils.IntegrationTestUtils;

public class WorkCountTestProccessorApi {

  @ClassRule
  public static final EmbeddedSingleNodeKafkaCluster CLUSTER = new EmbeddedSingleNodeKafkaCluster();

  private static String inputTopic = "inputTopic";
  private static String outputTopic = "outputTopic";

  @BeforeClass
  public static void startKafkaCluster() throws Exception {
    CLUSTER.createTopic(inputTopic);
    CLUSTER.createTopic(outputTopic);
  }

  @Test
  public void shouldCountWords() throws Exception {

    KStreamBuilder builder = new KStreamBuilder();

    IntegrationTestUtils.purgeLocalStreamsState(streamingProperties());

    StateStoreSupplier wordCountsStore = Stores.create("WordCountsStore")
        .withKeys(Serdes.String())
        .withValues(Serdes.Long())
        .persistent()
        .build();

    builder.addSource("SOURCE", inputTopic)
        .addProcessor("WORDS_COUNT",
            new WordCountProcessorSupplier(wordCountsStore.name()),
            "SOURCE")
        .addStateStore(wordCountsStore, "WORDS_COUNT")
        .addSink("OUTPUT",
            outputTopic,
            "WORDS_COUNT");


    KafkaStreams streams = new KafkaStreams(builder, streamingProperties());
    streams.start();

    IntegrationTestUtils.produceValuesSynchronously(inputTopic, inputValues(), producerProperties());

    List<KeyValue<String, Long>> actualValues =
        IntegrationTestUtils.waitUntilMinKeyValueRecordsReceived(
            consumerProperties(),
            outputTopic, expectedRecords().size());
    streams.close();
    assertThat(actualValues).isEqualTo(expectedRecords());
  }

  private List<String> inputValues() {
    return Arrays.asList(
          "foo",
          "bar",
          "foo",
          "quux",
          "bar",
          "foo");
  }

  private List<KeyValue<String, Long>> expectedRecords() {
    return Arrays.asList(
          new KeyValue<>("foo", 1L),
          new KeyValue<>("bar", 1L),
          new KeyValue<>("foo", 2L),
          new KeyValue<>("quux", 1L),
          new KeyValue<>("bar", 2L),
          new KeyValue<>("foo", 3L)
      );
  }

  private Properties streamingProperties() {
    Properties streamsConfiguration = new Properties();
    streamsConfiguration
        .put(StreamsConfig.APPLICATION_ID_CONFIG, "state-store-dsl-lambda-integration-test");
    streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
    streamsConfiguration.put(StreamsConfig.ZOOKEEPER_CONNECT_CONFIG, CLUSTER.zookeeperConnect());
    streamsConfiguration
        .put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());
    streamsConfiguration
        .put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, "/tmp/kafka-streams");
    return streamsConfiguration;
  }

  private Properties consumerProperties() {
    Properties consumerConfig = new Properties();
    consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
    consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG,
        "state-store-dsl-lambda-integration-test-standard-consumer");
    consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);
    return consumerConfig;
  }

  private Properties producerProperties() {
    Properties producerConfig = new Properties();
    producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
    producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
    producerConfig.put(ProducerConfig.RETRIES_CONFIG, 0);
    producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
    producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    return producerConfig;
  }


  private static final class WordCountProcessorSupplier
      implements ProcessorSupplier<byte[], String> {

    final private String stateStoreName;

    public WordCountProcessorSupplier(String stateStoreName) {
      this.stateStoreName = stateStoreName;
    }

    @Override
    public Processor<byte[], String> get() {
      return new Processor<byte[], String>() {

        private KeyValueStore<String, Long> stateStore;
        private ProcessorContext processorContext;

        @SuppressWarnings("unchecked")
        @Override
        public void init(ProcessorContext context) {
          stateStore = (KeyValueStore<String, Long>) context.getStateStore(stateStoreName);
          processorContext = context;
        }

        @Override public void process(byte[] key, String value) {
          Optional<Long> count = Optional.ofNullable(stateStore.get(value));
          Long incrementedCount = count.orElse(0L) + 1;
          stateStore.put(value, incrementedCount);
          processorContext.forward(value, incrementedCount);
          processorContext.commit();
        }

        @Override public void punctuate(long timestamp) {
          //no need it
        }

        @Override
        public void close() {
          stateStore.close();
        }
      };
    }

  }

}
