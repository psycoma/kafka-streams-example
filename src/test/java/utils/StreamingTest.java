package utils;

import kafka.EmbeddedSingleNodeKafkaCluster;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.rules.TemporaryFolder;

import java.util.Properties;

import static config.AppConfig.ENRICHED_USERS;
import static config.AppConfig.GEO_INPUT;
import static config.AppConfig.GEO_TO_ORDER;
import static config.AppConfig.ORDER_INPUT;
import static config.AppConfig.ORDER_TO_HISTORY;
import static config.AppConfig.USERINFO_INPUT;

public class StreamingTest {
  @ClassRule
  public static final EmbeddedSingleNodeKafkaCluster CLUSTER =
      new EmbeddedSingleNodeKafkaCluster();
  @ClassRule
  public static final TemporaryFolder folder = new TemporaryFolder();

  @BeforeClass
  public static void startKafkaCluster() throws Exception {
    CLUSTER.createTopic(GEO_INPUT);
    CLUSTER.createTopic(GEO_TO_ORDER);
    CLUSTER.createTopic(ORDER_TO_HISTORY);
    CLUSTER.createTopic(USERINFO_INPUT);
    CLUSTER.createTopic(ORDER_INPUT);
    CLUSTER.createTopic(ENRICHED_USERS);
  }

  public Properties getConsumerProperties(String groupId, Class jsonValueType) {
    Properties consumerConfig = new Properties();
    consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
    consumerConfig
        .put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
    consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
    consumerConfig.put("JSONType", jsonValueType);

    return consumerConfig;
  }

}
