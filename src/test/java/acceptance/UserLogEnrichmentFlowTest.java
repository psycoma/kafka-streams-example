package acceptance;

import domain.EnrichedUserLog;
import domain.OrderInfo;
import domain.UserLog;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.junit.Test;
import pipeline.GeoInformationPipeline;
import pipeline.UserHistoryPipeline;
import pipeline.UserOrdersPipeline;
import utils.IntegrationTestUtils;
import utils.StreamingTest;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static config.AppConfig.GEO_INPUT;
import static config.AppConfig.GEO_TO_ORDER;
import static config.AppConfig.GEO_TO_USERINFO;
import static config.AppConfig.HISTORY_INPUT;
import static config.AppConfig.HISTORY_TO_USERINFO;
import static config.AppConfig.ORDER_INPUT;
import static config.AppConfig.ORDER_TO_HISTORY;
import static config.AppConfig.USERINFO_INPUT;
import static config.AppConfig.USER_TO_ORDER_INPUT;
import static config.AppConfig.producerProperties;
import static config.AppConfig.streamingProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

public class UserLogEnrichmentFlowTest extends StreamingTest {

    @Test
    public void integrationTest() throws Exception {

        KafkaStreams userActivityStream = GeoInformationPipeline.construct(GEO_INPUT,
                GEO_TO_ORDER,
                GEO_TO_USERINFO,
                streamingProperties(CLUSTER.bootstrapServers(), UUID.randomUUID().toString()));

        KafkaStreams userOrdersStream = UserOrdersPipeline.construct(USER_TO_ORDER_INPUT,
                ORDER_INPUT,
                ORDER_TO_HISTORY,
                streamingProperties(CLUSTER.bootstrapServers(), UUID.randomUUID().toString()));

        KafkaStreams userHistoryStream = UserHistoryPipeline.construct(HISTORY_INPUT,
                HISTORY_TO_USERINFO,
                streamingProperties(CLUSTER.bootstrapServers(), UUID.randomUUID().toString()));

        publishActivityLogs();

        userActivityStream.start();

        userOrdersStream.start();

        userHistoryStream.start();

        publishOrders();

        List<KeyValue<String, EnrichedUserLog>> enrichedUserLogs =
                waitForEnrichedLogs();

        userActivityStream.close();

        EnrichedUserLog nullValue = userOrdersStream
                .store("userLog", QueryableStoreTypes.<String, EnrichedUserLog>keyValueStore())
                .get("transactionId");

        userOrdersStream.close();

        userHistoryStream.close();

        KeyValue<String, EnrichedUserLog> logWithoutTransactionId = enrichedUserLogs.get(0);
        KeyValue<String, EnrichedUserLog> logWithTransactionId = enrichedUserLogs.get(1);

        assertThat("Only record that has no transactionId should arrive",
                logWithoutTransactionId.value.getTransactionId(), is(nullValue()));
        assertThat("Geo information should be populated", logWithoutTransactionId.value.getCity(),
                is("Odessa"));
        assertThat("Key must be equal to userId", logWithoutTransactionId.key,
                is(logWithoutTransactionId.value.getUserId()));

        assertThat("Enriched record with order info should arrive",
                logWithTransactionId.value.getOrderPrice(),
                is("100"));

        assertThat("Enriched record should be removed from store after processing",
                nullValue,
                is(nullValue()));

        assertThat("Enriched record should contain orders history",
                logWithTransactionId.value.getUserOrdersHistory(),
                is(notNullValue()));

    }

    private List<KeyValue<String, EnrichedUserLog>> waitForEnrichedLogs() throws InterruptedException {
        return IntegrationTestUtils
                .waitUntilMinKeyValueRecordsReceived(
                        getConsumerProperties("enriched log with orders consumer",
                                EnrichedUserLog.class),
                        USERINFO_INPUT, 2);

    }

    private void publishOrders() throws ExecutionException, InterruptedException {
        IntegrationTestUtils.produceKeyValuesSynchronously(ORDER_INPUT,
                Arrays.asList(new KeyValue<>("transactionId", new OrderInfo("transactionId", "100"))),
                producerProperties(CLUSTER.bootstrapServers(), OrderInfo.class));
    }

    private void publishActivityLogs() throws ExecutionException, InterruptedException {
        IntegrationTestUtils.produceValuesSynchronously(GEO_INPUT,
                prepareLogs(),
                producerProperties(CLUSTER.bootstrapServers(), UserLog.class));
    }

    private Collection<UserLog> prepareLogs() {
        return Arrays.asList(new UserLog("Dmitry", "195.138.67.100", null),
                new UserLog("Vova", "195.138.67.100", "transactionId"));
    }

}
